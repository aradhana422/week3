//Converting string to lowerCase.

public class Assignment10 {
	public static String lowerCase(String str) {
		return str.toLowerCase();
	}

	public static void main(String[] args) {
		System.out.println(lowerCase("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));	

	}

}
