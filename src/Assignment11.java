//checking First and Last element of Array
public class Assignment11 {
public static boolean checkArray(Integer[] array1, Integer[] array2) {
		
		return (array1[0]==array2[0] && array1[array1.length-1]==array2[array2.length-1]);
		
	}


	public static void main(String[] args) {

		Integer[] array1 = {50,-20,0,30,40,60,12};
		Integer[] array2 = {45,20,10,20,30,50,11};
		
		System.out.println(checkArray(array1,array2));

}
}
