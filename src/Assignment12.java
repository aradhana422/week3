//Creating new Array.
public class Assignment12 {
	public static Integer[] newarray(Integer[] x, Integer[] y) {
		Integer[] a = new Integer[2];
		
		a[0] = x[0];
		a[1]=y[y.length-1];
		return a;
	}

	public static void main(String[] args) {
		
		Integer[] a = {50,-20,0};
		Integer[] b = {5,-50,10};
		Integer[] result = newarray(a,b);
		
		System.out.println(result[0] + " " + result[1]);


	}

}
