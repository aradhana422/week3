import java.util.Scanner;
public class Assignment16 {

	public static void main(String[] args) {

Scanner scan = new Scanner(System.in);
		
		System.out.print("Input first number: ");
		
		int first = scan.nextInt();
		
		System.out.print("Input second number: ");
		int second = scan.nextInt();
		scan.close();
		
		//Testing Data
		System.out.println("Sum of two integers: " + (first+second));
		System.out.println("Difference of two integers: " + (first-second));
		System.out.println("Product of two integers: " + first*second);
		System.out.println("Average of two integers: " + ((double)(first+second)/(double)2));
		System.out.println("Distance of two integers: " + (first-second));
		System.out.println("Max  integers: " + Math.max(first, second));
		System.out.println("Min integers: " + Math.min(first, second)); 
	     
	}

}
