import java.util.Arrays;
public class Assignment18 {
	public static Integer[] insertElement(Integer[] a, int position, int num) {
		Integer[] b = new Integer[a.length +1];
		
		
		
		for(int i = a.length; i> position; i--) {
			b[i] = a[i-1];
		}
		
		b[position] = num;
		
		//rest of the array
		for(int i = position; i> 0; i--) {
			b[i-1] = a[i-1];
		}
		
		
		return b;
	}


	public static void main(String[] args) {
		Integer[] elm = {12,23,28,12,56,62,78,82};
		System.out.println("Before inserting: " + Arrays.toString(elm));
		
		System.out.println("After inserting :" + Arrays.toString(insertElement(elm, 3,46)));
	

		
		

	}

}
