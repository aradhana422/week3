//Test for missingChar

public class Assignment2 {
	public static String missingChar(String str, int n) {
		String replaced = "";
		
		for(int i=0; i< str.length(); i++) {
			if(i!=n) {
			replaced += str.charAt(i);
			}
		}
		return replaced;
	}
	

	public static void main(String[] args) {
		
		System.out.println(missingChar("kitten",1));
		System.out.println(missingChar("kitten",0));
		System.out.println(missingChar("kitten",4));

		

	}

}
