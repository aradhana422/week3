
public class Assignment20 {

	public static void main(String[] args) {
		  String number1 = "Stephen Edwin King";
	        String number2 = "Walter Winchell";
	        String number3 = "Mike Royko";



	        boolean equals1 = number1.equals(number2);
	        boolean equals2 = number1.equals(number3);

	        
	        System.out.println("\"" + number1 + "\" equals \"" +
	            number2 + "\"? " + equals1);
	        System.out.println("\"" + number1 + "\" equals \"" +
	            number3 + "\"? " + equals2);
	

	}

}
